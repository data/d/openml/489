# OpenML dataset: dj30-1985-2003

https://www.openml.org/d/489

**WARNING: This dataset is archived. Use [meta](https://gitlab.com/data/meta) to discuss it.**

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.csv`](./dataset/tables/data.csv): CSV file with data
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

**Author**: Danilo Careggio (careggio.danilo@tiscali.it)  
**Source**: [StatLib](http://lib.stat.cmu.edu/datasets/) - 6/Feb/04  
**Please cite**:   

For each stock of Dow Jones 30, starting from 1985 up to and including Oct. 30th, 2003, daily quotations with open/close and adjusted close values, min./max values, volume are submitted. The said data are the result of a merge between different CSV files, downloaded from www.yahoo.com, as I made it to write my dissertation at the Dept. of Computer Science of University of Turin, Italy with Rosa Meo Prof. 

Data format is as follows: ID (Unique Identifier); date; open; high; low; volume; adjclose; ticker.

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/489) of an [OpenML dataset](https://www.openml.org/d/489). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/489/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/489/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/489/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

